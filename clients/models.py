from django.db import models


class Client(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=255, unique=True)
    birth_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.first_name + " " + self.last_name
