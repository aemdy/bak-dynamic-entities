import subprocess

import time
from django.conf import settings
from django.contrib import admin
from django.core.management import call_command
from django.apps import apps

from dynamics.models import DynamicEntity, DynamicEntityField, create_dynamic_models, FIELD_TYPE_STRING, FIELD_TYPE_FK
from dynamics.utils import get_dyn_config


class DynamicEntityFieldInline(admin.TabularInline):
    model = DynamicEntityField
    extra = 0


@admin.register(DynamicEntity)
class DynamicEntityAdmin(admin.ModelAdmin):
    inlines = [DynamicEntityFieldInline]
    list_display = ('verbose_name', 'verbose_name_plural', 'entity_object_list')
    search_fields = ('verbose_name', 'verbose_name_plural',)

    def entity_object_list(self, obj):
        url = '/admin/dynamics/%s/' % obj.verbose_name.lower()
        return '<a href="{}">list</a>'.format(url)

    entity_object_list.allow_tags = True
    entity_object_list.short_description = 'List URL'

    def _update_dynamics(self):
        DynamicEntity.export_entities()
        create_dynamic_models()
        call_command("makemigrations", "entities", verbosity=0)
        call_command("migrate", "--noinput", verbosity=0)
        subprocess.call(['touch', settings.WSGI_FILE])
        time.sleep(1)  # Wait until server is restarted

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        self._update_dynamics()

    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        self._update_dynamics()

def admin_from_entity(entity):
    fields = entity['fields']
    attrs = {
        "list_display": [field['name'] for field in fields],
        "search_fields": [field['name'] for field in fields
                          if field['type'] == FIELD_TYPE_STRING],
        "list_filter": [field['name'] for field in fields
                        if field['type'] == FIELD_TYPE_FK],
    }
    admin_class = type(entity['verbose_name'] + "Admin", (admin.ModelAdmin,), attrs)
    return admin_class


def create_dynamic_admins():
    config = get_dyn_config()

    for entity in config:
        admin_cls = admin_from_entity(entity)
        model = apps.get_model('entities', entity['verbose_name'])
        admin.site.register(model, admin_cls)


create_dynamic_admins()
