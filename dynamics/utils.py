import json

from django.conf import settings


def get_dyn_config():
    with open(settings.DYN_ENTITIES_FILE) as fp:
        return json.load(fp)
