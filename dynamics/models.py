import json
from importlib import import_module

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.text import slugify

from dynamics.utils import get_dyn_config

FIELD_TYPE_STRING = 0
FIELD_TYPE_INTEGER = 1
FIELD_TYPE_BOOLEAN = 2
FIELD_TYPE_DATE = 3
FIELD_TYPE_DATETIME = 4
FIELD_TYPE_FK = 5
FIELD_TYPES = (
    (FIELD_TYPE_STRING, 'String'),
    (FIELD_TYPE_INTEGER, 'Integer'),
    (FIELD_TYPE_BOOLEAN, 'Boolean'),
    (FIELD_TYPE_DATE, 'Date'),
    (FIELD_TYPE_DATETIME, 'Datetime'),
    (FIELD_TYPE_FK, 'Foreign key'),
)


def get_field_class(field_type):
    if field_type == FIELD_TYPE_STRING:
        return models.CharField
    elif field_type == FIELD_TYPE_INTEGER:
        return models.IntegerField
    elif field_type == FIELD_TYPE_BOOLEAN:
        return models.BooleanField
    elif field_type == FIELD_TYPE_DATE:
        return models.DateField
    elif field_type == FIELD_TYPE_DATETIME:
        return models.DateTimeField
    elif field_type == FIELD_TYPE_FK:
        return models.ForeignKey


class DynamicEntity(models.Model):
    verbose_name = models.CharField(max_length=30)
    verbose_name_plural = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = 'Dynamic entities'

    def __str__(self):
        return self.verbose_name

    @classmethod
    def export_entities(cls):
        entities = [e.to_json() for e in cls.objects.all()]
        with open(settings.DYN_ENTITIES_FILE, 'w') as fp:
            json.dump(entities, fp, indent=2)

    def to_json(self):
        return {
            'fields': [field.to_json() for field in self.fields.all()],
            'verbose_name': self.verbose_name,
            'verbose_name_plural': self.verbose_name_plural,
        }


class DynamicEntityField(models.Model):
    entity = models.ForeignKey(DynamicEntity, related_name='fields')
    name = models.CharField(max_length=20)
    type = models.PositiveSmallIntegerField(choices=FIELD_TYPES)
    attributes = JSONField(blank=True, default={})

    class Meta:
        unique_together = ('entity', 'name')

    def __str__(self):
        return self.entity.verbose_name + " " + self.name

    def to_json(self):
        return {
            'name': self.name,
            'type': self.type,
            'attributes': self.attributes,
        }


def model_from_entity(entity):
    class MetaClass(object):
        app_label = 'entities'
        db_table = '{}dynamics_{}'.format(settings.DYN_ENTITIES_TABLE_PREFIX,
                                          slugify(entity['verbose_name']))
        verbose_name = entity['verbose_name']
        verbose_name_plural = entity['verbose_name_plural']

    attrs = {
        '__module__': MetaClass.app_label,
        'Meta': MetaClass,
    }
    for field in entity['fields']:
        field_class = get_field_class(field['type'])
        attrs[field['name']] = field_class(**field['attributes'])
    new_model = type(entity['verbose_name'], (models.Model,), attrs)
    return new_model


def create_dynamic_models():
    config = get_dyn_config()
    for entity in config:
        model = model_from_entity(entity)
        pkg_models = import_module("dynamics.entities.models")
        setattr(pkg_models, entity['verbose_name'], model)


create_dynamic_models()
